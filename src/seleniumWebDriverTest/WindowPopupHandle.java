package seleniumWebDriverTest;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowPopupHandle {

	public static void main(String[] args) throws InterruptedException, IOException {

		System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.ilovepdf.com/");
			
		driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div[1]/a/div[1]")).click();
		driver.findElement(By.id("pickfiles")).click();
		Thread.sleep(2000);
		
		Runtime.getRuntime().exec("D:\\Drivers\\AutoITScript\\FileUpload ILove PDF.exe");
	}

}
