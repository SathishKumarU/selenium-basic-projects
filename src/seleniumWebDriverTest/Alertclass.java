package seleniumWebDriverTest;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alertclass {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
		WebDriver driver = new  ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_alert");
		
		driver.switchTo().frame("iframeResult");
		WebElement b = driver.findElement(By.xpath("/html/body/button"));
		b.click();
		
		Alert a = driver.switchTo().alert();
		System.out.println(a.getText());
		a.accept();
		
		
		
	}

}
