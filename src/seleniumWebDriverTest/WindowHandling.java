package seleniumWebDriverTest;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class WindowHandling {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.naukri.com/");
		
		WebElement Jobs = driver.findElement(By.xpath("//*[@id=\"root\"]/div[1]/div/ul[1]/li[1]/a/div"));
		
		Actions a = new Actions(driver);
		a.moveToElement(Jobs).build().perform();
		driver.findElement(By.xpath("//*[@id=\"root\"]/div[1]/div/ul[1]/li[1]/div/ul[1]/li[1]/a")).click();
		
		
		Set <String> windows = driver.getWindowHandles();
		Iterator <String> it = windows.iterator();
		String window1 = it.next();
		String window2 = it.next();
		
		driver.switchTo().window(window2);
		
		WebElement search = driver.findElement(By.id("qsb-keyword-sugg"));
		search.click();
		search.sendKeys("Selenium");
		driver.findElement(By.xpath("//*[@id=\"root\"]/div[3]/div[2]/section/div/form/div[3]/button")).click();
		
	}
}

