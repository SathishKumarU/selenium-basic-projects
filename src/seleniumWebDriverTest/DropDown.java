package seleniumWebDriverTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
		
		WebElement a = driver.findElement(By.id("select-demo"));
		
		Select b = new Select(a);
		
		
		if(a.isDisplayed()) {
			System.out.println("Displayed");
			if (a.isEnabled()) {
				System.out.println("Enabled");
				if (!a.isSelected()) {
					b.selectByIndex(0);
					b.selectByValue("Wednesday");
					b.selectByVisibleText("Friday");
					
					List<WebElement> ListItems = b.getOptions();
					for (WebElement List : ListItems) {
						System.out.println(List.getText());
				
				}
			}
		}
				
		//System.out.println(b.getOptions());
		
		
			
			
		}
		
	}

}
