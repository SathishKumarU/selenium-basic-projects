package seleniumWebDriverTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBox {

	public static void main(String[] args) {
		
	System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml5_input_type_checkbox");
	
	
	
	driver.switchTo().frame("iframeResult");
	WebElement Bike = driver.findElement(By.id("vehicle1"));
	WebElement Car = driver.findElement(By.id("vehicle2"));
	WebElement Boat = driver.findElement(By.id("vehicle3"));
	
	if (Bike.isDisplayed()) {
		System.out.println("Displayed");
	if (Bike.isEnabled()) {
		System.out.println("Enabled");
			if (!Bike.isSelected()) {
				Bike.click();
			}
		}
	}
	if (Car.isDisplayed()) {
		System.out.println("Displayed");
	if (Car.isEnabled()) {
		System.out.println("Enabled");
			if (!Car.isSelected()) {
				Car.click();
			}
		}
	}
	if (Boat.isDisplayed()) {
		System.out.println("Displayed");
	if (Boat.isEnabled()) {
		System.out.println("Enabled");
			if (!Boat.isSelected()) {
				Boat.click();
			}
		}
	}
	}
	}